#//////////////////////////////////////////////////
# Name: ipsend.py                                 /
# Programmer: Jason Blalock                       /
# Created: 01/25/13                               /
# Purpose: Send an email with LAN IP address(es)  /
#          of device                              /
#//////////////////////////////////////////////////

import time
import commands
import re
import smtplib
import sys

# CONFIG
SMTP_SERVER = 'smtp.gmail.com' #smtp server address
SMTP_PORT = 587 #port for smtp server

username = '' # gmail account
password = '' # gmail account password

sender = '' # address to send from (this can just be yourself)
recipient = '' # address to send to
subject = 'Raspberry Pi\'s IP Address' # subject of the message
body = 'Raspberry Pi\'s address: ' # message that is sent
# /CONFIG


# if the interface is wifi, it needs time to initialize, so delay before
# attempting to parse ifconfig
print('>> Waiting for network interfaces to initialize (30 seconds)')
time.sleep(30)

# extract the ip address(es) from ifconfig using regular expressions
valid_ips = []
ips = re.findall( r'[0-9]+(?:\.[0-9]+){3}', commands.getoutput("/sbin/ifconfig"))
for ip in ips:
    if ip.startswith("255") or ip.startswith("127") or ip.endswith("255"):
        continue
    valid_ips.append(ip)

body += ", ".join(valid_ips)

headers = ["From: " + sender,
           "Subject: " + subject,
           "To: " + recipient,
           "MIME-Version: 1.0",
           "Content-Type: text/html"]
headers = "\r\n".join(headers)

try:
    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    session.ehlo()
    session.starttls()
    session.ehlo()
except:
    print('Error: Could not connect to email host. Check internet connection.')
    sys.exit()
else:
    print('>> Connected to email host. Attempting secure login via SMTP...')
    try:
        session.login(username, password)
    except:
        print('Error: Could not secure connection. Stopping.')
        sys.exit()
    else:
        print('>> Login successful. Attempting to send message...')
        try:
            session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
        except TypeError as e:
            print e
            print ('Error:', sys.exc_info()[0])
            print ('Error: Could not send message. Check internet connection.')
            sys.exit()
        else:
            session.quit()
            print('>> Message successfully sent.')
            sys.exit()
